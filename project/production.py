try:
    from project.settings import *
except ImportError:
    pass

ALLOWED_HOSTS = [
    'tables.ddns.net',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tables',
        'USER': 'tables',
        'PASSWORD': 'qwerasdf',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}
