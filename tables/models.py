from django.db import connection, models
from django.db.models import Model, ForeignKey, CharField, IntegerField


def get_table_names():
    return {
        'ROW': Row._meta.db_table,
        Column.BOOLEAN: BooleanValue._meta.db_table,
        Column.INTEGER: IntegerValue._meta.db_table,
        Column.FLOAT: FloatValue._meta.db_table,
        Column.CHAR25: Char25Value._meta.db_table,
        Column.CHAR100: Char100Value._meta.db_table,
        Column.TEXT: TextValue._meta.db_table,
        Column.DATE: DateValue._meta.db_table,
        Column.DATETIME: DateTimeValue._meta.db_table,
        Column.REFERENCE: ReferenceValue._meta.db_table,
    }


class Table(Model):
    class Meta:
        verbose_name = 'Таблица'
        verbose_name_plural = 'Таблицы'
        ordering = ['name']

    def __str__(self):
        return '%s' % self.name

    def get_rows(self):
        columns = Column.objects.filter(table=self.id).all()

        with connection.cursor() as cursor:
            fields = []
            tables = []
            part1 = 'SELECT `{ROW}`.id AS id, '
            part2 = ' FROM `{ROW}` '
            part3 = ' WHERE `{ROW}`.table_id = %d' % self.id
            for c in columns:
                fields.append('T%d.value AS %s' % (c.id, c.identifier))
                cur_table = '`{%s}` AS T%d' % (c.column_type, c.id)
                condition = 'T%d.col_id = %d AND `{ROW}`.id = T%d.row_id' % (c.id, c.id, c.id)
                tables.append('LEFT JOIN ' + cur_table + ' ON ' + condition)

            query = part1 + ', '.join(fields) + part2 + ' '.join(tables) + part3
            cursor.execute(query.format(**get_table_names()))
            cols = ['id'] + [c.identifier for c in columns]
            rows = [dict(zip(cols, row)) for row in cursor.fetchall()]

        return rows

    identifier = CharField('Идентификатор', max_length=25)
    name = CharField('Наименование', max_length=100)


class Column(Model):
    class Meta:
        verbose_name = 'Колонка таблицы'
        verbose_name_plural = 'Колонки таблиц'
        ordering = ['table', 'order']

    def __str__(self):
        return '%s, column %s' % (self.table, self.identifier)

    def set_value(self, row_id, value):
        if self.column_type == Column.BOOLEAN:
            queryset = BooleanValue
        elif self.column_type == Column.INTEGER:
            queryset = IntegerValue
        elif self.column_type == Column.FLOAT:
            queryset = FloatValue
        elif self.column_type == Column.TEXT:
            queryset = TextValue
        elif self.column_type == Column.DATE:
            queryset = DateValue
        elif self.column_type == Column.DATETIME:
            queryset = DateValue
        elif self.column_type == Column.REFERENCE:
            queryset = DateValue
        elif self.column_type == Column.CHAR25:
            queryset = Char25Value
        elif self.column_type == Column.CHAR100:
            queryset = Char100Value
        else:
            return
        queryset.objects.update_or_create(col=self, row_id=row_id, defaults={'value': value})

    BOOLEAN = 'BOL'
    INTEGER = 'INT'
    FLOAT = 'FLT'
    TEXT = 'TXT'
    DATE = 'DAT'
    DATETIME = 'DTM'
    REFERENCE = 'REF'
    CHAR25 = 'C25'
    CHAR100 = 'C10'

    DATA_TYPE = (
        (BOOLEAN, 'Булево'),
        (INTEGER, 'Целое число'),
        (FLOAT, 'Вещественное число'),
        (CHAR25, 'Строка до 25 символов'),
        (CHAR100, 'Строка до 100 символов'),
        (TEXT, 'Текст'),
        (DATE, 'Дата'),
        (DATETIME, 'Дата и время'),
        (REFERENCE, 'Справочник'),
    )

    table = ForeignKey('Table', verbose_name='Таблица')
    order = IntegerField('Номер колонки')
    identifier = CharField('Идентификатор', max_length=25)
    full_name = CharField('Полное наименование', max_length=25)
    brief_name = CharField('Краткое наименование', max_length=25)
    column_type = CharField('Тип данных', max_length=3, choices=DATA_TYPE)
    reference = ForeignKey('Table', verbose_name='Справочник', related_name='ref', null=True, blank=True)


class Row(Model):
    class Meta:
        verbose_name = 'Строка таблицы'
        verbose_name_plural = 'Строки таблиц'

    def as_dict(self):
        columns = Column.objects.filter(table=self.table).all()

        with connection.cursor() as cursor:
            fields = []
            tables = []
            part1 = 'SELECT `{ROW}`.id AS id, '
            part2 = ' FROM `{ROW}` '
            part3 = ' WHERE `{ROW}`.id = %d' % self.id
            for c in columns:
                fields.append('T%d.value AS %s' % (c.id, c.identifier))
                cur_table = '`{%s}` AS T%d' % (c.column_type, c.id)
                condition = 'T%d.col_id = %d AND `{ROW}`.id = T%d.row_id' % (c.id, c.id, c.id)
                tables.append('LEFT JOIN ' + cur_table + ' ON ' + condition)

            query = part1 + ', '.join(fields) + part2 + ' '.join(tables) + part3
            cursor.execute(query.format(**get_table_names()))
            cols = ['id'] + [c.identifier for c in columns]
            row = cursor.fetchone()
            row = dict(zip(cols, row))

        return row

    @staticmethod
    def create_row(table, data):
        new_row = Row()
        new_row.table = table
        new_row.save()
        answer = dict(id=new_row.id)
        columns = Column.objects.filter(table=table).all()
        for c in columns:
            c.set_value(new_row.id, data.get(c.identifier))
            answer[c.identifier] = data.get(c.identifier)
        return answer

    def update(self, data):
        answer = dict(id=self.id)
        columns = Column.objects.filter(table=self.table).all()
        for c in columns:
            c.set_value(self.id, data.get(c.identifier))
            answer[c.identifier] = data.get(c.identifier)
        return answer

    def __str__(self):
        return '%s, id: %s' % (self.table, self.id)

    table = ForeignKey('Table', verbose_name='Таблица')


class Value(Model):
    class Meta:
        abstract = True

    col = ForeignKey('Column', verbose_name='Колонка')
    row = ForeignKey('Row', verbose_name='Строка')


class BooleanValue(Value):
    value = models.BooleanField('Значение')


class IntegerValue(Value):
    value = IntegerField('Значение')


class FloatValue(Value):
    value = models.FloatField('Значение')


class TextValue(Value):
    value = models.TextField('Значение')


class DateValue(Value):
    value = models.DateField('Значение')


class DateTimeValue(Value):
    value = models.DateTimeField('Значение')


class Char25Value(Value):
    value = CharField('Значение', max_length=25)


class Char100Value(Value):
    value = CharField('Значение', max_length=100)


class ReferenceValue(Value):
    value = IntegerField('Значение')
