from django.contrib import admin

from . import models

admin.site.register(models.Table)
admin.site.register(models.Column)
admin.site.register(models.Row)

admin.site.register(models.BooleanValue)
admin.site.register(models.IntegerValue)
admin.site.register(models.FloatValue)
admin.site.register(models.TextValue)
admin.site.register(models.Char25Value)
admin.site.register(models.Char100Value)
admin.site.register(models.DateValue)
admin.site.register(models.DateTimeValue)
admin.site.register(models.ReferenceValue)
