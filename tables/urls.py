from django.conf.urls import url
from django.contrib.auth.views import logout_then_login

from . import views
from . import views

urlpatterns = [
    url(r'^$', views.index_view, name='index'),
    url(r'^table/(?P<table_id>\d+)$', views.table_view, name='table'),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^logout/$', logout_then_login, name='logout'),

    url(r'^api/rows/(?P<table_id>\d+)/$', views.rows_api, name='rows_api'),
    url(r'^api/rows/(?P<table_id>\d+)/(?P<row_id>\d+)/$', views.rows_api, name='rows_api'),
]
