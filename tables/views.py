import json

from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404

from .forms import LoginForm
from .models import Table, Column, Row


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(**form.cleaned_data)
            if user is None:
                form.add_error(None, 'Неправильный логин или пароль')
            else:
                login(request, user)
                return redirect('index')
    else:
        form = LoginForm()
    return render(request, 'tables/login.html', {'form': form})


@login_required
def index_view(request):
    return render(request, 'tables/index.html', {
        'tables': Table.objects.all(),
    })


@login_required
def table_view(request, table_id):
    table = Table.objects.get(pk=table_id)
    return render(request, 'tables/table.html', {
        'table': table,
        'columns': Column.objects.all(),
        'rows': table.get_rows(),
    })


@login_required
def rows_api(request, table_id, row_id=None):
    if row_id is None:
        table = get_object_or_404(Table, pk=table_id)
        if request.method == 'GET':
            # Получить строки таблицы
            rows = table.get_rows()
            return HttpResponse(json.dumps(rows), content_type='application/json')
        elif request.method == 'POST':
            # Создать строку таблицы
            serialized_data = request.body.decode("utf-8")
            if serialized_data != '':
                new_row = Row.create_row(table, json.loads(serialized_data))
                return HttpResponse(json.dumps(new_row), content_type='application/json', status=201)
    else:
        row = get_object_or_404(Row, pk=row_id)
        if request.method == 'GET':
            # Получить строку таблицы
            return HttpResponse(json.dumps(row.as_dict()), content_type='application/json')
        elif request.method == 'PUT':
            # Изменить строку таблицы
            serialized_data = request.body.decode("utf-8")
            if serialized_data != '':
                updated_row = row.update(json.loads(serialized_data))
                return HttpResponse(json.dumps(updated_row), content_type='application/json')
        elif request.method == 'DELETE':
            # Удалить строку таблицы
            row.delete()
            return HttpResponse(json.dumps({'success': True}), content_type='application/json')
    return HttpResponse(json.dumps({'success': False}), content_type='application/json', status=404)
