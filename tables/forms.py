from django.forms import Form, CharField, PasswordInput, TextInput


class LoginForm(Form):
    """ Форма входа в систему """
    username = CharField(label='Логин', max_length=25, widget=TextInput())
    password = CharField(label='Пароль', max_length=25, widget=PasswordInput())
